var chakram = require('chakram');
var expect = chakram.expect;

var config = require('../config/config.json');

describe("API", function () {

    it("should confirm endpoint status", function () {
        var res = chakram.get(config.host + '/status');

        expect(res).to.have.status(200);
        expect(res).to.have.json(function(res) {
            expect(res).to.equal('Endpoint works.');
        });

        return chakram.wait();
    });

    it("should show http options", function () {
        var res = chakram.options(config.host + '/status');

        expect(res).to.have.status(200);
        expect(res).to.have.json(function(res) {
            expect(res).to.equal('Allowed methods: GET');
        });

        return chakram.wait();
    });

});

