var chakram = require('chakram');
var expect = chakram.expect;

var config = require('../config/config.json');

describe("User", function () {
    var token = '';

    it("should not show anything when unauthorized", function () {
        var res = chakram.get(config.host + '/user');
        expect(res).to.have.status(401);
        res = chakram.post(config.host + '/user/update');
        expect(res).to.have.status(401);

        return chakram.wait();
    });

    it("should not register user with invalid data", function () {
        var params = {email: "test@test.pl", password: "asd"};
        var res = chakram.post(config.host + '/user/register', params);
        expect(res).to.have.status(400);

        return chakram.wait();
    });

    it("should register user with valid data", function () {
        var params = {email: Math.random().toString(36).substr(2, 5) + "@test.pl", password: "test@test.pl"};
        var res = chakram.post(config.host + '/user/register', params);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.have.property('token');
            token = res.token;
        });

        return chakram.wait();
    });

    it("should show user authenticating with given token", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/user', options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.have.property('id');
            expect(res).to.have.property('email');
        });

        return chakram.wait();
    });

    it("should update user profile data", function () {
        var params = {first_name: "First", last_name: "Last"};
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        return chakram.patch(config.host + '/user', params, options)
            .then(function (res) {
                expect(res).to.have.status(200);
                return chakram.get(config.host + '/user', options);
            })
            .then(function (res) {
                expect(res).to.have.status(200);
                expect(res).to.have.json('first_name', 'First');
                expect(res).to.have.json('last_name', 'Last');
            });
    });

    it("should delete registered user", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/user/unregister', options);
        expect(res).to.have.status(200);

        return chakram.wait();
    });
});

