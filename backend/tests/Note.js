var chakram = require('chakram');
var expect = chakram.expect;

var config = require('../config/config.json');

describe("Note", function () {
    var token = '';
    var groupId = 0;
    var groupName = 'Testing group - ' + Math.random().toString(36).substr(2, 5);

    before("should register user with valid data", function () {
        var params = {email: Math.random().toString(36).substr(2, 5) + "@test.pl", password: "test@test.pl"};
        var res = chakram.post(config.host + '/user/register', params);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.have.property('token');
            token = res.token;
        });

        return chakram.wait();
    });

    before("should create new group", function () {
        var params = {name: groupName};
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.put(config.host + '/group', params, options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (group) {
            groupId = group.id;
        });

        return chakram.wait();
    });

    it("should not show anything when unauthorized", function () {
        var res = chakram.get(config.host + '/notes');
        expect(res).to.have.status(401);
        res = chakram.post(config.host + '/note/1');
        expect(res).to.have.status(401);

        return chakram.wait();
    });


    it("should show all my notes", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/note', options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.be.a('array');
        });

        return chakram.wait();
    });

    it("should create new note", function () {
        var params = {
            group_id: groupId,
            title: "Some title",
            content: "Something something."
        };
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.put(config.host + '/note', params, options);
        expect(res).to.have.status(200);

        return chakram.wait();
    });

    it("should not create note without title/content", function () {
        var params = {
            group_id: groupId,
            title: "",
            content: ""
        };
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.put(config.host + '/note', params, options);
        expect(res).to.have.status(400);

        return chakram.wait();
    });

    it("should create and delete note", function () {
        var params = {
            group_id: groupId,
            title: "Some title",
            content: "Something something."
        };
        var options = {headers: {'Authorization': 'Bearer ' + token}};

        return chakram.put(config.host + '/note', params, options)
            .then(function (res) {
                var noteId;

                expect(res).to.have.status(200);
                expect(res).to.have.json(function (group) {
                    expect(group).to.have.property('id');
                    noteId = group.id;
                });

                return chakram.delete(config.host + '/note/' + noteId, {}, options);
            })
            .then(function (res) {
                expect(res).to.have.status(200);
            });
    });

    after("should delete created group", function () {
        var params = {};
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.delete(config.host + '/group/' + groupId, params, options);
        expect(res).to.have.status(200);

        return chakram.wait();
    });

    after("should delete registered user", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/user/unregister', options);
        expect(res).to.have.status(200);

        return chakram.wait();
    });
});

