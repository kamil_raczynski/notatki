var chakram = require('chakram');
var expect = chakram.expect;

var config = require('../config/config.json');

describe("Group", function () {
    var token = '', otherToken = '';
    var groupId = 0;
    var groupName = 'Testing group - ' + Math.random().toString(36).substr(2, 5);

    before("should register user with valid data", function () {
        var params = {email: Math.random().toString(36).substr(2, 5) + "@test.pl", password: "test@test.pl"};
        var res = chakram.post(config.host + '/user/register', params);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.have.property('token');
            token = res.token;
        });

        params = {email: Math.random().toString(36).substr(2, 5) + "@test.pl", password: "test@test.pl"};
        res = chakram.post(config.host + '/user/register', params);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.have.property('token');
            otherToken = res.token;
        });

        return chakram.wait();
    });

    it("should not show anything when unauthorized", function () {
        var res = chakram.get(config.host + '/groups');
        expect(res).to.have.status(401);
        res = chakram.post(config.host + '/group/1');
        expect(res).to.have.status(401);
        res = chakram.post(config.host + '/group/1/notes');
        expect(res).to.have.status(401);
        res = chakram.post(config.host + '/group/1/users');
        expect(res).to.have.status(401);
        res = chakram.post(config.host + '/group/1/requests');
        expect(res).to.have.status(401);

        return chakram.wait();
    });

    it("should show all groups", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/group', options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.be.a('array');
        });

        return chakram.wait();
    });

    it("should show all my groups", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/groups/owned', options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.be.a('array');
        });

        return chakram.wait();
    });

    it("should create new group", function () {
        var params = {name: groupName};
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.put(config.host + '/group', params, options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (group) {
            groupId = group.id;
        });

        return chakram.wait();
    });

    it("should show all users from created group", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/group/' + groupId + '/users', options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.be.a('array');
        });

        return chakram.wait();
    });

    it("should show all notes from created group", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/group/' + groupId + '/notes', options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.be.a('array');
        });

        return chakram.wait();
    });

    it("should show all requests from created group", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/group/' + groupId + '/requests', options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.be.a('array');
        });

        return chakram.wait();
    });

    it("should show created group", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/group/' + groupId, options);
        expect(res).to.have.status(200);
        expect(res).to.have.json('id', groupId);
        expect(res).to.have.json('name', groupName);

        return chakram.wait();
    });

    it("should show created group on list", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/groups/owned', options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.be.a('array');
            expect(res).to.have.deep.property('[0].id', groupId);
            expect(res).to.have.deep.property('[0].name', groupName);
        });

        return chakram.wait();
    });

    it("should update group name", function () {
        var newName = 'New name';
        var params = {name: newName};
        var options = {headers: {'Authorization': 'Bearer ' + token}};

        return chakram.patch(config.host + '/group/' + groupId, params, options)
            .then(function (res) {
                expect(res).to.have.status(200);
                return chakram.get(config.host + '/group/' + groupId, {headers: {'Authorization': 'Bearer ' + token}});
            })
            .then(function (res) {
                expect(res).to.have.status(200);
                expect(res).to.have.json('name', newName);
                groupName = newName;
            });
    });

    it("should not allow request from group member", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.put(config.host + '/group/' + groupId + '/request', {}, options);
        expect(res).to.have.status(400);

        return chakram.wait();
    });

    it("should request group membership", function () {
        var options = {headers: {'Authorization': 'Bearer ' + otherToken}};
        var res = chakram.put(config.host + '/group/' + groupId + '/request', {}, options);
        expect(res).to.have.status(200);

        return chakram.wait();
    });

    it("should not request same group membership again", function () {
        var options = {headers: {'Authorization': 'Bearer ' + otherToken}};
        var res = chakram.put(config.host + '/group/' + groupId + '/request', {}, options);
        expect(res).to.have.status(400);

        return chakram.wait();
    });

    it("should show my request on list", function () {
        var options = {headers: {'Authorization': 'Bearer ' + otherToken}};
        var res = chakram.get(config.host + '/groups/requests', options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.be.a('array');
            expect(res).to.have.deep.property('[0].group_id', groupId);
        });

        return chakram.wait();
    });

    it("should not cancel request by different user", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.delete(config.host + '/group/' + groupId + '/request', {}, options);
        expect(res).to.have.status(400);

        return chakram.wait();
    });

    it("should cancel request", function () {
        var options = {headers: {'Authorization': 'Bearer ' + otherToken}};
        var res = chakram.delete(config.host + '/group/' + groupId + '/request', {}, options);
        expect(res).to.have.status(200);

        return chakram.wait();
    });

    it("should reject membership request", function () {
        var options = {headers: {'Authorization': 'Bearer ' + otherToken}};
        var requestId = 0;
        return chakram.put(config.host + '/group/' + groupId + '/request', {}, options)
            .then(function (res) {
                expect(res).to.have.status(200);
                expect(res).to.have.json(function (request) {
                    expect(request).to.have.property('id');
                    requestId = request.id;
                });
                return chakram.get(config.host + '/group/request/' + requestId + '/reject', {headers: {'Authorization': 'Bearer ' + token}});
            })
            .then(function (res) {
                expect(res).to.have.status(200);
            });
    });

    it("should accept membership request", function () {
        var options = {headers: {'Authorization': 'Bearer ' + otherToken}};
        var requestId = 0;
        return chakram.put(config.host + '/group/' + groupId + '/request', {}, options)
            .then(function (res) {
                expect(res).to.have.status(200);
                expect(res).to.have.json(function (request) {
                    expect(request).to.have.property('id');
                    requestId = request.id;
                });
                return chakram.get(config.host + '/group/request/' + requestId + '/accept', {headers: {'Authorization': 'Bearer ' + token}});
            })
            .then(function (res) {
                expect(res).to.have.status(200);
            });
    });

    it("should delete created group", function () {
        var params = {};
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.delete(config.host + '/group/' + groupId, params, options);
        expect(res).to.have.status(200);

        return chakram.wait();
    });

    it("should not show created group anymore", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/group', options);
        expect(res).to.have.status(200);
        expect(res).to.have.json(function (res) {
            expect(res).to.be.a('array');
            expect(res).to.not.deep.include.members([{ id: groupId, name: groupName }]);
        });

        return chakram.wait();
    });

    after("should delete registered user", function () {
        var options = {headers: {'Authorization': 'Bearer ' + token}};
        var res = chakram.get(config.host + '/user/unregister', options);
        expect(res).to.have.status(200);

        chakram.get(config.host + '/user/unregister', {headers: {'Authorization': 'Bearer ' + otherToken}});
        expect(res).to.have.status(200);

        return chakram.wait();
    });
});

