<?php

namespace App\View;

use App\Model\GroupRequest;

class GroupRequestView extends View
{
    /**
     * @param GroupRequest $object
     * @return array
     */
    public function showOne($object)
    {
        return [
            'id' => $object->getId(),
            'group_id' => $object->getGroup()->getId(),
            'applicant_id' => $object->getApplicant()->getId(),
            'requested' => $object->getRequested()->format('Y.m.d H:i:s'),
        ];
    }
}