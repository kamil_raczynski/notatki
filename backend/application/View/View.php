<?php

namespace App\View;

abstract class View
{
    /**
     * @param $object
     * @return array
     */
    abstract public function showOne($object);

    /**
     * @param array $objects
     * @return array
     */
    public function showMany($objects)
    {
        $entities = [];

        foreach ($objects as $object) {
            $entities[] = $this->showOne($object);
        }

        return $entities;
    }
}