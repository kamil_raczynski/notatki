<?php

namespace App\View;

use App\Model\Note;

class NoteView extends View
{
    /**
     * @param Note $object
     * @return array
     */
    public function showOne($object)
    {
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'content' => $object->getContent(),
            'group_id' => $object->getGroup()->getId(),
            'author_id' => $object->getAuthor()->getId(),
            'created' => $object->getCreated()->format('Y.m.d H:i:s'),
        ];
    }
}