<?php

namespace App\View;

use App\Model\Group;

class GroupView extends View
{
    /**
     * @param Group $object
     * @return array
     */
    public function showOne($object)
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'created' => $object->getCreated()->format('Y.m.d H:i:s'),
        ];
    }
}