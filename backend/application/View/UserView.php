<?php

namespace App\View;

use App\Model\User;

class UserView extends View
{
    /**
     * @param User $object
     * @return array
     */
    public function showOne($object)
    {
        return [
            'id' => $object->getId(),
            'email' => $object->getEmail(),
            'first_name' => $object->getFirstName(),
            'last_name' => $object->getLastName(),
            'created' => $object->getCreated()->format('Y.m.d H:i:s'),
        ];
    }
}