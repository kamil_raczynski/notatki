<?php

namespace App\Core\Exception;

class ValidationException extends \Exception
{
    /** @var array */
    private $messages;

    /**
     * @param array $messages
     */
    public function __construct(array $messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }
}