<?php

namespace App\Core;

use App\Model\User;
use Firebase\JWT\JWT;

class Authorization
{
    static public function generateTokenFor(User $user)
    {
        return JWT::encode([
            'user' => [
                'id' => $user->getId(),
                'email' => $user->getEmail()
            ]
        ], 'top-kek');
    }
}