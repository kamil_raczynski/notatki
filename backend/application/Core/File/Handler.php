<?php

namespace App\Core\File;

use Upload\File;
use Upload\Storage\FileSystem;
use Upload\Validation\Mimetype;
use Upload\Validation\Size;

class Handler
{
    /** @var array */
    private $files;

    /** @var string */
    private $name;

    /** @var string */
    private $filename;

    public function __construct($name, array $files)
    {
        $this->name = $name;
        $this->files = $files;
    }

    public function handle()
    {
        $path = ROOT . '/public/files/' . $this->name;
        if (!file_exists($path)) {
            mkdir($path);
        }

        $file = new File('file', new FileSystem($path));

        $file->setName(uniqid());
        $file->addValidations([
            new Mimetype(['image/png', 'image/gif', 'image/jpeg']),
            new Size('5M'),
        ]);

        try {
            $this->filename = $file->getNameWithExtension();
            $file->upload();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getFilename()
    {
        return $this->filename;
    }
}