<?php

namespace App\Core;

use App\Controller\Controller;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\InvocationStrategyInterface;

class RoutingStrategy implements InvocationStrategyInterface
{
    public function __invoke(callable $callable, ServerRequestInterface $request, ResponseInterface $response, array $routeArguments)
    {
        /** @var Controller $controller */
        $controller = $callable[0];
        $controller->setRequest($request);
        $controller->setResponse($response);

        try {
            $result = call_user_func_array(
                $callable,
                $routeArguments
            );

            return $controller->handleResult($result);
        } catch (\Exception $exception) {
            return $controller->handleException($exception);
        }
    }
}