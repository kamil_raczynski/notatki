<?php

namespace App\Core\Error;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Monolog\Logger;
use Slim\Handlers\Error;
use Slim\Http\Body;

final class Handler extends Error
{
    protected $logger;
    protected $output;

    public function __construct(Logger $logger, $throwToOutput = false)
    {
        $this->logger = $logger;
        $this->output = $throwToOutput;
    }

    public function __invoke(Request $request, Response $response, \Exception $exception)
    {
        $this->logger->critical($exception->getMessage());

        if ($this->output) {
            $body = json_encode([
                'error' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => $exception->getTraceAsString()
            ], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        } else {
            $body = null;
        }

        return $response
            ->withStatus(500)
            ->withHeader('Content-type', 'application/json')
            ->withBody(new Body(fopen('php://temp', 'r+')))
            ->write($body);
    }
}