<?php

namespace App\Validation;

use Ayeo\Validator\Constraint\MinLength;
use Ayeo\Validator\ValidationRules;

class NoteRules extends ValidationRules
{
    function getRules()
    {
        return [
            ['title', new MinLength(1)],
            ['content', new MinLength(1)],
        ];
    }
}