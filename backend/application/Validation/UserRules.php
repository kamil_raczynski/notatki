<?php

namespace App\Validation;

use Ayeo\Validator\Constraint\Email;
use Ayeo\Validator\Constraint\MinLength;
use Ayeo\Validator\ValidationRules;

class UserRules extends ValidationRules
{
    private $hasNoPassword;

    /**
     * @param $hasNoPassword
     */
    public function __construct($hasNoPassword)
    {
        $this->hasNoPassword = $hasNoPassword;
    }


    function getRules()
    {
        $rules = [
            ['email', new Email()],
        ];
        if ($this->hasNoPassword) {
            $rules[] = ['passwordProvided', new MinLength(1)];
        }
        return $rules;
    }
}