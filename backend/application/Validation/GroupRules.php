<?php

namespace App\Validation;

use Ayeo\Validator\Constraint\MinLength;
use Ayeo\Validator\ValidationRules;

class GroupRules extends ValidationRules
{
    function getRules()
    {
        return [
            ['name', new MinLength(1)],
        ];
    }
}