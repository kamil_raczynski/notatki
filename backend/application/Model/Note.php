<?php

namespace App\Model;

use App\Validation\NoteRules;
use App\Core\Exception\ValidationException;
use Ayeo\Validator\Validator;

/**
 * @Entity
 * @Table(name="notes")
 * @HasLifecycleCallbacks
 **/
class Note
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string", length=255)
     */
    protected $title;

    /**
     * @Column(type="string", length=4096)
     */
    protected $content;

    /**
     * @ManyToOne(targetEntity="App\Model\User")
     */
    protected $author;

    /**
     * @ManyToOne(targetEntity="App\Model\Group")
     */
    protected $group;

    /**
     * @Column(type="string", length=255, nullable=true)
     */
    protected $filename;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $created;

    /**
     * @param string $title
     * @param string $content
     * @param User $author
     * @param Group $group
     */
    public function __construct($title, $content, User $author, Group $group)
    {
        $this->title = $title;
        $this->content = $content;
        $this->author = $author;
        $this->group = $group;
        $this->created = new \DateTime();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return bool
     */
    public function hasPicture()
    {
        return empty($this->filename) === false;
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function validate()
    {
        $validator = new Validator(new NoteRules());
        if ($validator->validate($this) === false)
        {
            throw new ValidationException($validator->getErrors());
        }
    }
}