<?php

namespace App\Model;

use App\Core\Exception\ValidationException;
use App\Validation\GroupRules;
use Ayeo\Validator\Validator;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="groups_requests")
 * @HasLifecycleCallbacks
 **/
class GroupRequest
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="App\Model\User")
     */
    protected $applicant;

    /**
     * @ManyToOne(targetEntity="App\Model\Group")
     */
    protected $group;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $requested;

    /**
     * @param User $applicant
     * @param Group $group
     */
    public function __construct(User $applicant, Group $group)
    {
        $this->applicant = $applicant;
        $this->group = $group;
        $this->requested = new \DateTime();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getApplicant()
    {
        return $this->applicant;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return \DateTime
     */
    public function getRequested()
    {
        return $this->requested;
    }
}