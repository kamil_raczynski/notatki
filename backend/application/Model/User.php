<?php

namespace App\Model;

use App\Core\Exception\ValidationException;
use App\Validation\UserRules;
use Ayeo\Validator\Validator;

/**
 * @Entity
 * @Table(name="users")
 * @HasLifecycleCallbacks
 **/
class User
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string", length=255, unique=true) **/
    protected $email;

    /** @Column(type="string", length=32) **/
    protected $password;
    protected $passwordProvided;

    /** @Column(type="string", length=255, name="first_name", options={"default":""}) **/
    protected $firstName;

    /** @Column(type="string", length=255, name="last_name", options={"default":""}) **/
    protected $lastName;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $created;

    public function __construct($email, $password = '')
    {
        $this->email = $email;
        $this->setPassword($password);
        $this->created = new \DateTime();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param $password
     * @return bool
     */
    public function verify($password)
    {
        return md5($password) === $this->password;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = md5($password);
        $this->passwordProvided = $password;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName ? $firstName : '';
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName ? $lastName : '';
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getPasswordProvided() //todo :/
    {
        return $this->passwordProvided;
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function validate()
    {
        $validator = new Validator(new UserRules(empty($this->password)));
        if ($validator->validate($this) === false)
        {
            throw new ValidationException($validator->getErrors());
        }
    }
}