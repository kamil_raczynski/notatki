<?php

namespace App\Model;

use App\Core\Exception\ValidationException;
use App\Validation\GroupRules;
use Ayeo\Validator\Validator;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="groups")
 * @HasLifecycleCallbacks
 **/
class Group
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ManyToOne(targetEntity="App\Model\User")
     */
    protected $owner;

    /**
     * @ManyToMany(targetEntity="App\Model\User")
     */
    protected $members;

    /**
     * @OneToMany(targetEntity="App\Model\Note", mappedBy="group", orphanRemoval=true)
     */
    protected $notes;

    /**
     * @OneToMany(targetEntity="App\Model\GroupRequest", mappedBy="group", orphanRemoval=true)
     */
    protected $requests;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $created;

    /**
     * @param $name
     * @param User $owner
     */
    public function __construct($name, User $owner)
    {
        $this->name = $name;
        $this->owner = $owner;
        $this->notes = new ArrayCollection();
        $this->requests = new ArrayCollection();
        $this->members = new ArrayCollection();
        $this->members->add($owner);
        $this->created = new \DateTime();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @return ArrayCollection|User[]
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @return ArrayCollection|Note[]
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @return ArrayCollection|GroupRequest[]
     */
    public function getRequests()
    {
        return $this->requests;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function validate()
    {
        $validator = new Validator(new GroupRules());
        if ($validator->validate($this) === false)
        {
            throw new ValidationException($validator->getErrors());
        }
    }
}