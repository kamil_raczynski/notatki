<?php

namespace App\Controller;

use App\Core\Exception\ValidationException;
use App\Model\Group;
use App\Model\Note;
use App\View\NoteView;

class NoteController extends Controller
{
    public function getAll()
    {
        $notes = $this->getRepository('Note')->findBy([
            'author' => $this->getUser()
        ]);

        return $this->getNoteView()->showMany($notes);
    }

    public function get($id)
    {
        /** @var Note $note */
        $note = $this->getRepository('Note')->findOneBy(['id' => $id]);

        $isAuthor = $note->getAuthor()->getId() === $this->getUser()->getId();
        $isMember = $note->getGroup()->getMembers()->contains($this->getUser());

        if ($isAuthor || $isMember) {
            return $this->getNoteView()->showOne($note);
        } else {
            return $this->getResponse()->withStatus(400);
        }
    }

    public function create()
    {
        /** @var Group $group */
        $group = $this->getRepository('Group')->find($this->param('group_id', 0));

        if ($group) {
            $filename = $this->handleFile('note');
            if ($filename || $this->hasFileToHandle() === false) {
                $note = new Note($this->param('title'), $this->param('content'), $this->getUser(), $group);

                if ($filename) {
                    $note->setFilename($filename);
                }

                $this->getEntityManager()->persist($note);
                $this->getEntityManager()->flush();

                return $this->getNoteView()->showOne($note);
            } else {
                return $this->getResponse()->withStatus(400);
            }
        } else {
            throw new ValidationException(['group_id' => 'not_found']);
        }
    }

    public function edit($id)
    {
        /** @var \App\Model\Note $note */
        $note = $this->getRepository('Note')->find($id);

        $isAuthor = $note && $this->getUser()->getId() === $note->getAuthor()->getId();

        if ($isAuthor) {
            $note->setTitle($this->param('title'));
            $note->setContent($this->param('content'));

            $this->getEntityManager()->persist($note);
            $this->getEntityManager()->flush();

            return $this->getResponse()->withStatus(200);
        } else {
            return $this->getResponse()->withStatus(400);
        }
    }

    public function delete($id)
    {
        try {
            /** @var \App\Model\Note $note */
            $note = $this->getRepository('Note')->find($id);

            $isAuthor = $note && $this->getUser()->getId() === $note->getAuthor()->getId();

            if ($isAuthor) {
                $this->getEntityManager()->remove($note);
                $this->getEntityManager()->flush();

                return $this->getResponse()->withStatus(200);
            } else {
                return $this->getResponse()->withStatus(400);
            }
        } catch (\Exception $e) {
            return $this->getResponse()->withStatus(400);
        }
    }

    private function getNoteView()
    {
        return new NoteView();
    }
}
