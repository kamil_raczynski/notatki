<?php

namespace App\Controller;

use App\Core\Authorization;
use App\Core\Exception\ValidationException;
use App\Model\User;
use App\View\UserView;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class UserController extends Controller
{
    public function index()
    {
        return $this->getUserView()->showOne($this->getUser());
    }

    public function login()
    {
        $user = $this->getRepository('User')->findOneBy([
            'email' => $this->param('email')
        ]);

        $isAuthorized = $user && $user->verify($this->param('password'));

        if ($isAuthorized) {
            return [
                'token' => Authorization::generateTokenFor($user)
            ];
        } else {
            return $this->getResponse()->withStatus(401);
        }
    }

    public function register()
    {
        $user = new User($this->param('email'), $this->param('password'));
        $user->setFirstName($this->param('first_name', ''));
        $user->setLastName($this->param('last_name', ''));

        try {
            $this->getEntityManager()->persist($user);
            $this->getEntityManager()->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new ValidationException(['email' => 'non_unique']);
        }

        return [
            'token' => Authorization::generateTokenFor($user)
        ];
    }

    public function logout()
    {
        // todo not implemented
    }

    public function unregister()
    {
        $email = $this->getUser()->getEmail();

        $user = $this->getRepository('User')->findOneBy(['email' => $email]);

        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }

    public function edit()
    {
        $user = $this->getUser();

        $user->setFirstName($this->param('first_name'));
        $user->setLastName($this->param('last_name'));

        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    private function getUserView()
    {
        return new UserView();
    }
}
