<?php

namespace App\Controller;

use App\Model\Group;
use App\Model\GroupRequest;
use App\View\GroupRequestView;
use App\View\GroupView;
use App\View\NoteView;
use App\View\UserView;

class GroupController extends Controller
{
    public function getAll()
    {
        $groups = $this->getRepository('Group')->findAll();

        return $this->getGroupView()->showMany($groups);
    }

    public function getAllOwned()
    {
        $groups = $this->getRepository('Group')->findBy([
            'owner' => $this->getUser()
        ]);

        return $this->getGroupView()->showMany($groups);
    }

    public function getAllRequests()
    {
        /** @var GroupRequest[] $requests */
        $requests = $this->getRepository('GroupRequest')->findBy([
            'applicant' => $this->getUser(),
        ]);

        return $this->getGroupRequestView()->showMany($requests);
    }

    public function get($id)
    {
        /** @var Group $group */
        $group = $this->getRepository('Group')->findOneBy([
            'id' => $id,
            'owner' => $this->getUser(),
        ]);

        return $this->getGroupView()->showOne($group);
    }

    public function getGroupNotes($id)
    {
        /** @var Group $group */
        $group = $this->getRepository('Group')->find($id);

        $isOwner = $group && $group->getOwner()->getId() === $this->getUser()->getId();
        $isMember = $group && $group->getMembers()->contains($this->getUser());

        if ($isOwner || $isMember) {
            return $this->getNoteView()->showMany($group->getNotes());
        } else {
            return $this->getResponse()->withStatus(400);
        }
    }

    public function getGroupUsers($id)
    {
        /** @var Group $group */
        $group = $this->getRepository('Group')->find($id);

        $isOwner = $group && $group->getOwner()->getId() === $this->getUser()->getId();
        $isMember = $group && $group->getMembers()->contains($this->getUser());

        if ($isOwner || $isMember) {
            return $this->getUserView()->showMany($group->getMembers());
        } else {
            return $this->getResponse()->withStatus(400);
        }
    }

    public function getGroupRequests($id)
    {
        /** @var Group $group */
        $group = $this->getRepository('Group')->find($id);

        $isOwner = $group && $group->getOwner()->getId() === $this->getUser()->getId();
        $isMember = $group && $group->getMembers()->contains($this->getUser());

        if ($isOwner || $isMember) {
            return $this->getGroupRequestView()->showMany($group->getRequests());
        } else {
            return $this->getResponse()->withStatus(400);
        }
    }

    public function create()
    {
        $group = new Group($this->param('name'), $this->getUser());

        $this->getEntityManager()->persist($group);
        $this->getEntityManager()->flush();

        return $this->getGroupView()->showOne($group);
    }

    public function requestMembership($groupId)
    {
        try {
            /** @var Group $group */
            $group = $this->getRepository('Group')->find($groupId);
            if (!$group)
                throw new \Exception();

            $isMember = $group->getMembers()->contains($this->getUser());
            if ($isMember)
                throw new \Exception("Jesteś już w tej grupie");

            $request = $this->getRepository('GroupRequest')->findOneBy([
                'applicant' => $this->getUser(),
                'group' => $group,
            ]);
            if ($request)
                throw new \Exception("Nie można aplikować do grupy więcej niż jednokrotnie");

            $request = new GroupRequest($this->getUser(), $group);
            $this->getEntityManager()->persist($request);
            $this->getEntityManager()->flush();

            return $this->getGroupRequestView()->showOne($request);
        } catch (\Exception $e) {
            return $this->getResponse()->withStatus(400)->write($e->getMessage());
        }
    }

    public function acceptMembershipRequest($requestId)
    {
        try {
            /** @var GroupRequest $request */
            $request = $this->getRepository('GroupRequest')->find($requestId);
            if (!$request) throw new \Exception();

            $isOwner = $request->getGroup()->getOwner()->getId() === $this->getUser()->getId();

            if ($isOwner) {
                $applicant = $request->getApplicant();

                $group = $request->getGroup();
                $group->getMembers()->add($applicant);

                $this->getEntityManager()->persist($group);
                $this->getEntityManager()->remove($request);
                $this->getEntityManager()->flush();
            } else {
                $this->getResponse()->withStatus(400)->write("Akceptować podanie może wyłącznie założyciel grupy");
            }
        } catch (\Exception $e) {
            return $this->getResponse()->withStatus(400);
        }
    }

    public function rejectMembershipRequest($requestId)
    {
        try {
            /** @var GroupRequest $request */
            $request = $this->getRepository('GroupRequest')->find($requestId);
            if (!$request) throw new \Exception();

            $isOwner = $request->getGroup()->getOwner()->getId() === $this->getUser()->getId();

            if ($isOwner) {
                $this->getEntityManager()->remove($request);
                $this->getEntityManager()->flush();
            } else {
                $this->getResponse()->withStatus(400)->write("Odrzucić podanie może wyłącznie założyciel grupy");
            }
        } catch (\Exception $e) {
            return $this->getResponse()->withStatus(400);
        }
    }

    public function cancelMembershipRequest($groupId)
    {
        try {
            /** @var Group $group */
            $group = $this->getRepository('Group')->find($groupId);
            if (!$group) throw new \Exception();

            $request = $this->getRepository('GroupRequest')->findOneBy([
                'applicant' => $this->getUser(),
                'group' => $group,
            ]);

            if ($request) {
                $this->getEntityManager()->remove($request);
                $this->getEntityManager()->flush();

                return $this->getResponse()->withStatus(200);
            } else {
                return $this->getResponse()->withStatus(400);
            }
        } catch (\Exception $e) {
            return $this->getResponse()->withStatus(400);
        }
    }

    public function edit($id)
    {
        try {
            /** @var Group $group */
            $group = $this->getRepository('Group')->find($id);

            $isOwner = $group && $this->getUser()->getId() === $group->getOwner()->getId();

            if ($isOwner) {
                $group->setName($this->param('name'));

                $this->getEntityManager()->persist($group);
                $this->getEntityManager()->flush();

                return $this->getResponse()->withStatus(200);
            } else {
                return $this->getResponse()->withStatus(400);
            }
        } catch (\Exception $e) {
            return $this->getResponse()->withStatus(400);
        }
    }

    public function delete($id)
    {
        try {
            /** @var Group $group */
            $group = $this->getRepository('Group')->find($id);

            if ($group && $this->getUser()->getId() === $group->getOwner()->getId()) {
                $this->getEntityManager()->remove($group);
                $this->getEntityManager()->flush();

                return $this->getResponse()->withStatus(200);
            } else {
                return $this->getResponse()->withStatus(400);
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            return $this->getResponse()->withStatus(400);
        }
    }

    private function getGroupView()
    {
        return new GroupView();
    }

    private function getGroupRequestView()
    {
        return new GroupRequestView();
    }

    private function getNoteView()
    {
        return new NoteView();
    }

    private function getUserView()
    {
        return new UserView();
    }
}
