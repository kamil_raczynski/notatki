<?php

namespace App\Controller;

use App\Core\Exception\ValidationException;
use App\Core\File\Handler;
use App\Model\User;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class Controller
{
    /** @var ContainerInterface */
    protected $container;

    /** @var Request */
    private $request;

    /** @var Response */
    private $response;

    /** @var EntityManager */
    private $entityManager;

    /** @var User */
    private $user;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->entityManager = $container->get('entity_manager');

        if ($container->has('user')) {
            $this->user = $container->get('user');
        } else {
            $this->user = new User('Guest');
        }
    }

    /**
     * @param ServerRequestInterface $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @param ResponseInterface $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Response
     */
    protected function getResponse()
    {
        return $this->response;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param string $entity
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository($entity)
    {
        return $this->entityManager->getRepository('App\\Model\\' . $entity);
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    protected function param($name, $default = null)
    {
        return $this->getRequest()->getParam($name, $default);
    }

    /**
     * @param mixed $response
     * @return Response
     */
    public function handleResult($response)
    {
        if ($response instanceof Response) {
            return $response;
        }

        if (gettype($response) === 'string') {
            return $this->getResponse()->write($response);
        }

        if (is_null($response)) {
            return $this->getResponse()->withStatus(200);
        }

        try {
            return $this->getResponse()->withJson($response);
        } catch (\RuntimeException $e) {
            return $this->getResponse()
                ->withStatus(500)
                ->write(sprintf('%s: %s', $e->getCode(), $e->getMessage()));
        }
    }

    /**
     * @param \Exception $exception
     * @return Response
     */
    public function handleException(\Exception $exception)
    {
        if ($exception instanceof ValidationException) {
            return $this
                ->getResponse()
                ->withStatus(400)
                ->withJson($exception->getMessages());
        }

        return $this
            ->getResponse()
            ->withStatus(500);
    }

    protected function hasFileToHandle()
    {
        return empty($this->getRequest()->getUploadedFiles()) === false;
    }

    protected function handleFile($name)
    {
        if ($this->hasFileToHandle() === false) {
            return false;
        }

        $handler = new Handler($name, $this->getRequest()->getUploadedFiles());

        if ($handler->handle()) {
            return $handler->getFilename();
        } else {
            return false;
        }
    }

    protected function deleteFile($filename)
    {
        unlink(ROOT . '/public/files/' . $filename);
    }
}