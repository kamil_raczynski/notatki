<?php

define('ROOT', __DIR__ . '/..');

$loader = require __DIR__.'/../vendor/autoload.php';
$config = require __DIR__.'/../config/config.php';

$isDevMode = true;
$setup = Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(["application/Model"], $isDevMode);

$app = $config['app'];
$app['entity_manager'] = Doctrine\ORM\EntityManager::create($config['database'], $setup);

$container = new \Slim\Container($app);
$container['logger'] = function($container) use ($config) {
    $fingersCrossed = new Monolog\Handler\FingersCrossedHandler(
        new Monolog\Handler\StreamHandler($config['errors']['path'], $config['errors']['level']),
        Monolog\Logger::ERROR
    );

    $logger = new Monolog\Logger('error');
    $logger->pushHandler($fingersCrossed);

    return $logger;
};
$container['errorHandler'] = function ($container) use($config) {
    return new App\Core\Error\Handler($container['logger'], $config['errors']['output']);
};

$app = new \Slim\App($container);

$container = $app->getContainer();
$container['foundHandler'] = function() {
    return new \App\Core\RoutingStrategy();
};

$auth = [
    "secure" => false,
    "secret" => "top-kek",
    "algorithm" => ["HS256"],
    "rules" => [
        new \Slim\Middleware\JwtAuthentication\RequestPathRule([
            "path" => ['/'],
            "passthrough" => ['/status', '/user/login', '/user/register']
        ]),
        new \Slim\Middleware\JwtAuthentication\RequestMethodRule([
            "path" => ['/'],
            "passthrough" => ['OPTIONS']
        ])
    ],
    'callback' => function ($request, $response, $arguments) use ($container) {
        $id = $arguments['decoded']->user->id;

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $container['entity_manager'];

        $user = $em->getRepository('App\\Model\\User')->find($id);
        if ($user) {
            $container['user'] = $user;
        } else {
            return false;
        }
    },
];

$app->add(new Slim\Middleware\JwtAuthentication($auth));

$app->add(function (Slim\Http\Request $request, Slim\Http\Response $response, callable $next) {
    $uri = $request->getUri();
    $path = $uri->getPath();

    if ($path != '/' && substr($path, -1) == '/') {
        return $response->withRedirect((string) $uri->withPath(substr($path, 0, -1)), 301);
    }

    return $next($request, $response);
});

$app->add(function (Slim\Http\Request $request, Slim\Http\Response $response, callable $next) {
    /** @var Slim\Http\Response $response */
    $response = $next($request, $response);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
});

$app->get('/status', 'App\\Controller\\StatusController:index');

$app->get('/user', 'App\\Controller\\UserController:index');
$app->get('/user/logout', 'App\\Controller\\UserController:logout');
$app->get('/user/unregister', 'App\\Controller\\UserController:unregister');
$app->patch('/user', 'App\\Controller\\UserController:edit');
$app->post('/user/login', 'App\\Controller\\UserController:login');
$app->post('/user/register', 'App\\Controller\\UserController:register');

$app->get('/note[s]', 'App\\Controller\\NoteController:getAll');
$app->get('/note/{id:[0-9]+}', 'App\\Controller\\NoteController:get');
$app->map(['put', 'post'], '/note', 'App\\Controller\\NoteController:create');
$app->patch('/note/{id:[0-9]+}', 'App\\Controller\\NoteController:edit');
$app->delete('/note/{id:[0-9]+}', 'App\\Controller\\NoteController:delete');

$app->get('/group[s]', 'App\\Controller\\GroupController:getAll');
$app->get('/groups/owned', 'App\\Controller\\GroupController:getAllOwned');
$app->get('/group/{id:[0-9]+}', 'App\\Controller\\GroupController:get');
$app->get('/group/{id:[0-9]+}/notes', 'App\\Controller\\GroupController:getGroupNotes');
$app->get('/group/{id:[0-9]+}/users', 'App\\Controller\\GroupController:getGroupUsers');
$app->get('/group/{id:[0-9]+}/requests', 'App\\Controller\\GroupController:getGroupRequests');
$app->put('/group', 'App\\Controller\\GroupController:create');
$app->patch('/group/{id:[0-9]+}', 'App\\Controller\\GroupController:edit');
$app->delete('/group/{id:[0-9]+}', 'App\\Controller\\GroupController:delete');

$app->get('/groups/requests', 'App\\Controller\\GroupController:getAllRequests');
$app->get('/group/request/{id:[0-9]+}/accept', 'App\\Controller\\GroupController:acceptMembershipRequest');
$app->get('/group/request/{id:[0-9]+}/reject', 'App\\Controller\\GroupController:rejectMembershipRequest');
$app->put('/group/{id:[0-9]+}/request', 'App\\Controller\\GroupController:requestMembership');
$app->delete('/group/{id:[0-9]+}/request', 'App\\Controller\\GroupController:cancelMembershipRequest');

$app->options('/{.+}', function ($request, $response, $args) {
    return $response;
});

return $app;
