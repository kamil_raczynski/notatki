<?php

return [
    'app' => [
        'settings' => [
            'displayErrorDetails' => false,
        ]
    ],
    'database' => [
        'driver' => 'pdo_mysql',
        'host' => 'localhost',
        'user' => '__USER_MYSQL__',
        'password' => '__HASLO_MYSQL__',
        'dbname' => '__BAZA_MYSQL__',
    ],
    'authentication' => [
        "secure" => false,
        "secret" => "top-kek",
        "algorithm" => ["HS256"],
        "rules" => [
            new \Slim\Middleware\JwtAuthentication\RequestPathRule([
                "path" => ['/'],
                "passthrough" => ['/status']
            ]),
            new \Slim\Middleware\JwtAuthentication\RequestMethodRule([
                "path" => ['/user/login', '/user/register'],
                "passthrough" => ['POST']
            ]),
            new \Slim\Middleware\JwtAuthentication\RequestMethodRule([
                "path" => ['/'],
                "passthrough" => ['OPTIONS']
            ])
        ]
    ]
];