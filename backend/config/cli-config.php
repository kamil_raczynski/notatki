<?php

//ini_set('display_errors', true);
//ini_set('error_reporting', E_ALL);

$config = require __DIR__.'/config.php';

$setup = Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(["application/Model"], true);
$entityManager = Doctrine\ORM\EntityManager::create($config['database'], $setup);

return Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);