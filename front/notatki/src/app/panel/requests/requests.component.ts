import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {RequestsService} from "./requests.service";

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {

  requests = [];
  groups = [];

  constructor(
    private route: ActivatedRoute,
    private requestsService: RequestsService
  ) { }

  ngOnInit() {
    this.groups = this.route.snapshot.data['ownGroups'];
    this.groups.forEach((element, index) => {
      this.getRequestsForGroup(element.id, index);
    })
  }

  declineRequest(id:number) {
    this.requestsService.declineRequest(id).subscribe(
      () => {
        alert("Request have been declined");
        location.reload();
      },
      (error) => {
        alert(error.statusText);
        location.reload();

      }
    )
  }

  acceptRequest(id:number) {
    this.requestsService.acceptRequest(id).subscribe(
      (data) => {
        alert("Request have been accepted");
        location.reload();
      },
      (error) => {
        alert(error.statusText);
        location.reload();
      }
    )
  }

  getRequestsForGroup(id:number, index) {
    this.requestsService.getRequestsForGroup(id).subscribe(
      (data) => {
        if (data.length) {
          this.requests[index] = data;
        }else{
          this.requests[index] = null;
        }
        return data;
      }
    )
  }



}
