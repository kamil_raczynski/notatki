import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import {AuthenticationService} from "app/auth/index";
import {ConfigService} from "app/core/config.service";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class RequestsService {

  private defaultHeader:Headers;
  private token:string;
  private apiUrl:string;

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService,
    private configService: ConfigService
  ) {
    this.token = this.authenticationService.token;
    this.defaultHeader = new Headers({
      'Authorization': 'Bearer ' + this.token,
    });
    this.apiUrl = this.configService.getConfig().apiUrl;
  }

  getRequests(): Observable<any> {
    return this.http.get(`${this.apiUrl}/groups/requests`, {headers: this.defaultHeader})
      .map(result => result.json());
  };

  getRequestsForGroup(id:number): Observable<any> {
    return this.http.get(`${this.apiUrl}/group/${id}/requests`, {headers: this.defaultHeader})
      .map(res => res.json());

  }

  declineRequest(id:number): Observable<any> {
    return this.http.get(`${this.apiUrl}/group/request/${id}/reject`, {headers: this.defaultHeader})
      .map(response => response);
  }

  acceptRequest(id:number): Observable<any> {
    return this.http.get(`${this.apiUrl}/group/request/${id}/accept`, {headers: this.defaultHeader})
      .map(response => response);
  }

  sendRequest(id:number):Observable<any> {
    let headers = new Headers(
      {
        'Authorization': 'Bearer ' + this.authenticationService.token,
        'Content-Type': 'application/json'
      }
    );
    return this.http.put(`${this.apiUrl}/group/${id}/request`, {} ,{headers: headers})
      .map(result => result.json());
  }


}
