import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { RequestsService } from './requests.service';

@Injectable()
export class RequestsResolve implements Resolve<any> {

  constructor(private requestsService: RequestsService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.requestsService.getRequests();
  }
}
