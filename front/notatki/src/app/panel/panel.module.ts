import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './panel.component';
import { routing } from './panel.routing';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NotesService } from './notes/index';
import { NotesComponent } from './notes/index';
import { NotesResolve } from './notes/index';
import { Note } from './notes/index';
import { GroupsComponent } from './groups/index';
import {GroupsService} from "./groups/index";
import {GroupsResolve} from "./groups/index";
import {Group} from "./groups/index";
import {OwnGroupsResolve} from "./groups/ownGroups.resolve";
import { NewNoteComponent } from './notes/index';
import {FormsModule} from "@angular/forms";
import { EditNoteComponent } from './notes/index';
import { GroupComponent } from './groups/index';
import { NewGroupComponent } from './groups/index';
import { RequestsComponent } from './requests/index';
import {RequestsService} from "./requests/index";
import {RequestsResolve} from "./requests/index";




@NgModule({
  imports: [
    CommonModule,
    routing,
    FormsModule,
  ],
  declarations: [
    PanelComponent,
    SidebarComponent,
    NotesComponent,
    GroupsComponent,
    NewNoteComponent,
    EditNoteComponent,
    GroupComponent,
    NewGroupComponent,
    RequestsComponent
  ],
  providers: [
    NotesService,
    NotesResolve,
    Note,
    GroupsService,
    GroupsResolve,
    Group,
    RequestsService,
    RequestsResolve,
    OwnGroupsResolve
]
})
export class PanelModule { }
