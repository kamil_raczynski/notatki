import { Routes, RouterModule } from '@angular/router';

import { PanelComponent } from './panel.component';
import { NotesComponent } from './notes/index';
import { GroupsComponent } from './groups/index';
import { NotesResolve } from './notes/index';
import {GroupsResolve} from "./groups/index";
import {NewNoteComponent} from "./notes/index";
import {EditNoteComponent} from "./notes/index";
import {GroupComponent} from "./groups/index";
import {UserResolve} from "../user/user.resolve";
import {NewGroupComponent} from "./groups/index";
import {RequestsComponent} from "./requests/index";
import {RequestsResolve} from "./requests/index";
import {OwnGroupsResolve} from "./groups/ownGroups.resolve";

const panelRoutes: Routes = [
    {
        path: '',
        component: PanelComponent,
        children: [
            {
              path: '',
              redirectTo: 'notes',
              pathMatch: 'full'
            },
            {
              path: 'notes',
              component: NotesComponent,
                resolve: {
                    notes: NotesResolve
                }
            },
            {
              path: 'note/:id',
                component: EditNoteComponent,
                resolve: {
                  groups: GroupsResolve,
                  user: UserResolve
                }
            },
            {
              path: 'notes/new',
              component: NewNoteComponent,
              resolve: {
                ownGroups: OwnGroupsResolve
              }
            },
            {
              path: 'groups',
              component: GroupsComponent,
              resolve: {
                groups: GroupsResolve
              }
            },
            {
              path: 'groups/new',
              component: NewGroupComponent
            },
            {
              path: 'group/:id/:title',
              component: GroupComponent
            },
            {
              path: 'requests',
              component: RequestsComponent,
              resolve: {
                requests: RequestsResolve,
                // groupsWithRequests: GroupsWithRequestsResolve,
                ownGroups: OwnGroupsResolve
              }
            }
        ]
    },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forChild(panelRoutes);
