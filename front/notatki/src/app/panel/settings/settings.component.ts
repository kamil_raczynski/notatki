import { Component, OnInit } from '@angular/core';
import {User} from "../../user/user";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  model = {};

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.model = this.route.snapshot.data['user'];
  }

}
