import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {NotesService} from "../notes/notes.service";
import {Note} from "../notes/note";
import {RequestsService} from "../requests/index";

@Component({
  selector: 'panel-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {

  groups:any = [];
  notes:Array<Note> = [];

  constructor(
    private route: ActivatedRoute,
    private notesService: NotesService,
    private requestsService: RequestsService
  ) { }

  ngOnInit(){
    this.groups = this.route.snapshot.data['groups'];
    this.getNotes();
  }

  getNotes() {
    this.notesService.getNotes()
      .subscribe(
        (data) => {
          this.notes = data
        }
      )
  }

  sendRequest(id:number) {
    this.requestsService.sendRequest(id)
      .subscribe(
        () => {
          alert("Request have been send");
        },
        (error) => {
          alert(error.statusText)
        }
      )
  }

}
