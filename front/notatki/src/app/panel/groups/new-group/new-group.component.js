"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var NewGroupComponent = (function () {
    function NewGroupComponent(groupsService, router) {
        this.groupsService = groupsService;
        this.router = router;
        this.model = {};
        this.error = '';
    }
    NewGroupComponent.prototype.ngOnInit = function () { };
    NewGroupComponent.prototype.createGroup = function () {
        var _this = this;
        this.groupsService.newGroup(this.model).subscribe(function () {
            _this.router.navigate(['/panel/groups']);
        }, function (error) {
            _this.error = error.statusText;
        });
    };
    NewGroupComponent = __decorate([
        core_1.Component({
            selector: 'app-new-group',
            templateUrl: './new-group.component.html',
            styleUrls: ['./new-group.component.scss']
        })
    ], NewGroupComponent);
    return NewGroupComponent;
}());
exports.NewGroupComponent = NewGroupComponent;
