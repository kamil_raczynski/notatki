import { Component, OnInit } from '@angular/core';
import {GroupsService} from "../groups.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.scss']
})
export class NewGroupComponent implements OnInit {

  model:any = {};
  error:string = '';

  constructor(
    private groupsService: GroupsService,
    private router: Router
  ) {}

  ngOnInit() {}


  createGroup() {
    this.groupsService.newGroup(this.model).subscribe(
      () => {
        this.router.navigate(['/panel/groups']);
      },
      (error) => {
        this.error = error.statusText;
      }
    )
  }

}
