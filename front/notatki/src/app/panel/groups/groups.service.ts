import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Group } from './index';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map'
import {AuthenticationService} from "app/auth/index";
import {ConfigService} from "app/core/config.service";

@Injectable()
export class GroupsService {

  apiUrl:string;
  token:string;
  defaultHeader:Headers;

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService,
    private configService: ConfigService
  ) {
    this.apiUrl = this.configService.getConfig().apiUrl;
    this.token = this.authenticationService.token;
    this.defaultHeader = new Headers({
      'Authorization': 'Bearer ' + this.token,
      'Content-Type': 'application/json'
    })
  }

  getGroups(): Observable<Group> {
    return this.http.get(`${this.apiUrl}/groups`, {headers: this.defaultHeader})
      .map(response => response.json());
  }

  getOwnGroups(): Observable<Group> {
    return this.http.get(`${this.apiUrl}/groups/owned`, {headers: this.defaultHeader})
      .map(response => response.json());
  }

  newGroup(groupModel):any {
    return this.http.put(`${this.apiUrl}/group`, groupModel, {headers: this.defaultHeader})
      .map(response => response.json())
  }

}
