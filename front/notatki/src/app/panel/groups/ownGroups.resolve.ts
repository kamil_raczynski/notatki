import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { GroupsService } from './index';


@Injectable()
export class OwnGroupsResolve implements Resolve<any> {

  constructor(
    private groupService: GroupsService,
  ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.groupService.getOwnGroups();
  }
}
