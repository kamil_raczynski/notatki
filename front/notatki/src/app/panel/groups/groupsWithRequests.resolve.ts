import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { GroupsService } from './index';
import {RequestsService} from "../requests/index";


@Injectable()
export class GroupsWithRequestsResolve implements Resolve<any> {

  constructor(
    private groupService: GroupsService,
    private requestsService: RequestsService
  ) {}

  resolve(route: ActivatedRouteSnapshot) {
    let requestsService = this.requestsService;
    let getRequests = this.getRequests;



    return this.groupService.getOwnGroups().subscribe(
      group => {
        // return group.map(function(group) {
        //   group.requests = getRequests(group.id, requestsService);
        //   return group;
        // });
        return group;
      },
      err => err
    );
  }

  getRequests(id:number, requestsService) {
     return requestsService.getRequestsForGroup(id)
       .subscribe(
         (data) => {
           console.log("DATA Z REQUESTU");
           console.log(data);
         }
       );
  }




}
