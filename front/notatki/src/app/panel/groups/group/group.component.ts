import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {NotesService} from "../../notes/notes.service";
import 'rxjs/add/operator/switchMap';
import {Group} from "./group";
import {Note} from "../../notes/note";


@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  groupId:number;
  groupTitle:string;
  notes:Array<Note>;
  group:Group;

  constructor (
    private notesService: NotesService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => {
        this.groupId = +params['id'];
        this.groupTitle = params['title'];
        return this.notesService.getNotes();
      })
      .subscribe((notes: Array<Note>) => this.notes = notes)
  }

}
