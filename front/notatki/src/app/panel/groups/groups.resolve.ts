import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { GroupsService } from './index';

@Injectable()
export class GroupsResolve implements Resolve<any> {

  constructor(private groupService: GroupsService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.groupService.getGroups();
  }
}
