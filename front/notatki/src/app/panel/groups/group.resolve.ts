import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { GroupsService } from './index';
import {UserService} from "../../user/user.service";

@Injectable()
export class GroupResolve implements Resolve<any> {

  constructor(
    private groupService: GroupsService,
    private usersService: UserService
  ){}

  resolve(route: ActivatedRouteSnapshot) {
    // console.log(this.usersService.getGroupUsers(route.params.id));
    // return this.usersService.getGroupUsers(route.params.id);
  }
}
