import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Note } from './note';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {

  notes:Array<Note>;
  constructor(
      private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.notes = this.route.snapshot.data['notes'];
  }



}
