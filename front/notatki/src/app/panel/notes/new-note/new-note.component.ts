import { Component, OnInit } from '@angular/core';
import {NotesService} from "../notes.service";
import { Router, ActivatedRoute } from "@angular/router";
import {GroupsService} from "app/panel/groups/index";

@Component({
  selector: 'app-new-note',
  templateUrl: './new-note.component.html',
  styleUrls: ['./new-note.component.scss']
})
export class NewNoteComponent implements OnInit {

  model:any = {};
  error:string = '';
  groups:any = [];

  constructor(
    private notesService: NotesService,
    private groupsService: GroupsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    //this.groupsService.getOwnGroups().subscribe(
    //  data => {
    //    this.groups = data;
    //  }
    //)
    this.groups = this.route.snapshot.data['ownGroups'];
  }

  newNote() {
    this.notesService.newNote(this.model).subscribe(
      () => {
        this.router.navigate(['/panel/notes']);
      },
      (error) => {
        this.error = error.statusText;
      }
    );
  }

}

