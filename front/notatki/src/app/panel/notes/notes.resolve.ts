import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { NotesService } from './notes.service';

@Injectable()
export class NotesResolve implements Resolve<any> {

    constructor(private noteService: NotesService) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.noteService.getNotes();
    }
}
