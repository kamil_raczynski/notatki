import {Injectable} from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { Note } from './note';
import {AuthenticationService} from "../../auth/index";
import 'rxjs/add/operator/map';
import {UserService} from "../../user/user.service";
import {ConfigService} from "../../core/config.service";


@Injectable()
export class NotesService{

  apiUrl:string;
  token:string;
  defaultHeader:Headers;

  constructor(
      private http: Http,
      private authenticationService: AuthenticationService,
      private configService: ConfigService,
      private userService: UserService
  ) {
      this.apiUrl = this.configService.getConfig().apiUrl;
      this.token = this.authenticationService.token;
      this.defaultHeader = new Headers({
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      })
  }

  getNotes(): Observable<Note[]> {
    return this.http.get(`${this.apiUrl}/notes`, {headers: this.defaultHeader}).map(
      (response: Response) => response.json());
  }

  getNote(id): Observable<Note[]> {
    return this.http.get(`${this.apiUrl}/note/${id}`, {headers: this.defaultHeader}).map(
      (response) => {
        return response.json();
      }
    )
  }

  newNote(form): Observable<Response> {
    let userId = this.userService.getUser();
    form.author_id = userId;

    return this.http.put(`${this.apiUrl}/note`, form, {headers: this.defaultHeader})
      .map((response) => response);
  }

  updateNote(form): Observable<Response> {
    return this.http.patch(`${this.apiUrl}/note/${form.id}`, form, {headers: this.defaultHeader})
      .map((response) => response);
  }

  deleteNote(id): Observable<Response> {
    return this.http.delete(`${this.apiUrl}/note/${id}`, {headers: this.defaultHeader})
      .map((response) => response);
  }
}
