export class Note {
    id: number;
    title:  string;
    content: string;
    group_id: number;
    author_id: number;
}
