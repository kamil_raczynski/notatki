import { Component, OnInit } from '@angular/core';
import {NotesService} from "../notes.service";
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import {Note} from "../note";
import {User} from "../../../user/user";


@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.scss']
})
export class EditNoteComponent implements OnInit {

  model:any = {
    author: {}
  };
  user:User;
  error:string = '';
  noteId:number = null;
  groups:any = [];
  editMode:boolean = false;

  constructor(
    private notesService: NotesService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.groups = this.route.snapshot.data['groups'];

    this.user = this.route.snapshot.data['user'];

    this.route.params
      .switchMap((params: Params) => {
        this.noteId = +params['id'];
        return this.notesService.getNote(this.noteId);
      })
      .subscribe((note: any) => this.model = note);
  }

  updateNote() {
    this.notesService.updateNote(this.model).subscribe(
      () => {
        this.router.navigate(['/panel/notes']);
      },
      (error) => {
        this.error = error.statusText;
      }
    )
  }

  getNoteGroupName(id:number):string {
    let group = this.groups.filter(group => group.id === id)[0];
    let groupName:string = group ? group.name : "Note does not belongs to any group";
    return groupName;
  }


  deleteNote() {
    this.notesService.deleteNote(this.noteId)
      .subscribe(
        (data) => {
          this.router.navigate(['/panel/notes']);
        },
        (error) => {
          this.error = error.statusText;
        }
      )
  }

  canEdit():boolean {
    return this.user.id === this.model.author.id;
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
  }

}
