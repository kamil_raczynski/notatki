import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserService} from "../../user/user.service";
import {User} from "../../user/user";

@Component({
  selector: 'panel-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {

  user:any = {};

  constructor(
    public router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.userService.getUser().subscribe(
      data => {
        return this.user = data;
      }
    )
  }



}
