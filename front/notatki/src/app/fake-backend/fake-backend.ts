import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import {Note} from "../panel/notes/index";
import {User} from "../user/user";
import {Group} from "app/panel/groups/index";
import {Author} from "../user/author";

export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: Http,
  useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
    // configure fake backend
    backend.connections.subscribe((connection: MockConnection) => {
      console.log(connection);
      let testUser:User = {
        id: 0,
        email: 'test',
        password: 'test',
        first_name: 'Kamil',
        last_name: 'Raczyński',
        full_name: 'Kamil Raczyński'
      };
      let authors:Array<Author> = [
        {
          id: 0,
          first_name: 'Kamil',
          last_name: 'Raczyński'
        },
        {
          id: 1,
          first_name: 'Inny',
          last_name: 'Autor'
        }
      ];
      let notes:Array<Note> = [
        {
          id: 0,
          title: 'Pierwszy tytuł notatki',
          content: 'Zawartość pierwszej notatki',
          group_id: 1,
          author_id: 0
        },
        {
          id: 1,
          title: 'Drugi tytuł notatki',
          content: 'Zawartość pierwszej notatki',
          group_id: 1,
          author_id: 1
        },
        {
          id: 2,
          title: 'Trzeci tytuł notatki',
          content: 'Zawartość pierwszej notatki',
          group_id: 1,
          author_id: 1
        },
        {
          id: 3,
          title: 'Czwarty tytuł notatki',
          content: 'Zawartość pierwszej notatki',
          group_id: 2,
          author_id: 0
        },
        {
          id: 4,
          title: 'Piąty tytuł notatki',
          content: 'Zawartość pierwszej notatki',
          group_id: 2,
          author_id: 0
        },
        {
          id: 5,
          title: "Szósta notatka",
          content: "FAJNA ZAWARTOŚ",
          group_id: 0,
          author_id: 0
        }
      ];

      let groups:Array<Group> = [
        {
          id: 0,
          name: "ZEROWA GRUPA"
        },
        {
          id: 1,
          name: "Piewsza grupa"
        },
        {
          id: 2,
          name: "Druga grupa"
        }
      ];

      setTimeout(() => {
        // AUTHORIZATION
        // fake authenticate api end point
        if (connection.request.url.endsWith('/login') && connection.request.method === RequestMethod.Post) {
          // get parameters from post request
          console.log(connection);
          let params = JSON.parse(connection.request.getBody());

          // check user credentials and return fake jwt token if valid
          if (params.email === testUser.email && params.password === testUser.password) {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200, body: { token: 'fake-jwt-token' } })
            ));
          } else {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200 })
            ));
          }
        }

        // USER
        if (connection.request.url.endsWith('/user') && connection.request.method === RequestMethod.Get) {
          console.log("poszed user");
          // check for fake auth token in header and return test users if valid, this security is implemented server side
          // in a real application
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            console.log("If przeszedl");
            console.log(testUser);
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200, body: testUser })
            ));
          } else {
            // return 401 not authorised if token is null or invalid
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 401 })
            ));
          }
        }

        // AUTHORS
        if (connection.request.url.endsWith('/authors') && connection.request.method === RequestMethod.Get) {
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            console.log("Poszed ten connection");
            console.log(authors);
            connection.mockRespond(new Response(
              new ResponseOptions({status: 200, body: authors})
            ));
          }
        }

        // NOTES
        if (connection.request.url.endsWith('/notes') && connection.request.method === RequestMethod.Get) {
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            connection.mockRespond(new Response(
              new ResponseOptions({status: 200, body: notes})
            ));
          }
        }

        if (connection.request.url.endsWith('/note/') && connection.request.method === RequestMethod.Get) {
          console.log("UDAŁ SIĘ?");
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            connection.mockRespond(new Response(
              new ResponseOptions({status: 200, body: notes})
            ));
          }
        }

        // GROUPS

        if (connection.request.url.endsWith('/groups') && connection.request.method === RequestMethod.Get) {
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            connection.mockRespond(new Response(
              new ResponseOptions({status: 200, body: groups})
            ));
          }
        }


      }, 500);

    });

    return new Http(backend, options);
  },
  deps: [MockBackend, BaseRequestOptions]
};
