import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AuthenticationService } from '../auth/index';
import { User } from './user';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {ConfigService} from "../core/config.service";

@Injectable()
export class UserService {

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService,
    private configService: ConfigService
  ) {}

  getUser(): Observable<User> {
    let headers: Headers = new Headers(
      {
        'Authorization': 'Bearer ' + this.authenticationService.token,
        'Content-Type': 'application/json'
      }
    );
    return this.http.get(`${this.configService.getConfig().apiUrl}/user`, {headers: headers}).map(
      (response: Response) => response.json());
  }


}
