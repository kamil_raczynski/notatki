import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {

  private config = {
    apiUrl: 'http://notatki.dev:1026'
    // apiUrl: ''
  };

  constructor() {}

  getConfig() {
    return this.config;
  }

}
