import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponent } from './core.component';
import {ConfigService} from "./config.service";

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    ConfigService
  ],
  declarations: [CoreComponent]
})
export class CoreModule { }
