import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';

import { fakeBackendProvider } from './fake-backend/fake-backend';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './auth/index';

import {AuthGuard} from "./auth/index";
import {AuthenticationService} from "./auth/index";
import {UserService} from './user/user.service';
import {UserResolve} from './user/user.resolve';
import { RegistrationComponent } from './auth/index';
import {SharedModule} from "./shared/shared.module";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SharedModule,
    routing,
  ],
  providers: [
    AuthGuard,
    //services
    AuthenticationService,
    UserService,
    //fake backend
    // fakeBackendProvider,
    // MockBackend,
    // BaseRequestOptions,
    //resolves
    UserResolve
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
