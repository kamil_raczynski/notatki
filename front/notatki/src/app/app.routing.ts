import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './auth/index';
import { AuthGuard } from './auth/index';
import {RegistrationComponent} from "./auth/index";

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegistrationComponent
  },
  {
    path: '',
    redirectTo: '/panel/notes',
    pathMatch: 'full'
  },
  {
    path: 'panel',
    canActivate: [AuthGuard],
    loadChildren: 'app/panel/panel.module#PanelModule'
  },
  {
    path: '**',
    redirectTo: ''
  }
];

export const routing = RouterModule.forRoot(appRoutes);
