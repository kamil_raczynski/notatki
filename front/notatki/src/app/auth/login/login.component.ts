import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Http, Headers, Response } from '@angular/http';
import { AuthenticationService } from '../authentication.service';
import {ConfigService} from "../../core/config.service";

@Component({
  // moduleId: module.id,
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit {
  model:any = {};
  loading:boolean = false;
  error:string = '';
  coreConfig:any = {};

  headers:Headers = new Headers();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private configService: ConfigService,
    public http: Http
  ) {
    this.coreConfig = this.configService.getConfig();
  }

  ngOnInit() {
    this.authenticationService.logout();
  }


  login() {
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe((result) => {
        if (result === true) {
          this.router.navigate(['/panel']);
        }else{
          this.error = 'Username or password is incorrect';
          this.loading = false;
        }
      });
  }

}
