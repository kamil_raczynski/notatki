import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { Http, Headers, Response } from '@angular/http';
import { AuthenticationService } from '../authentication.service';



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  model:any = {};
  loading:boolean = false;
  error:string = '';

  headers:Headers = new Headers();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    public http: Http
  ) { }

  ngOnInit() {
    this.authenticationService.logout();
  }

  register() {
    this.authenticationService.register(this.model)
      .subscribe((result) => {
          if (result === true) {
            this.router.navigate(['/panel']);
          }else{
            this.error = 'Data is invalid';
            this.loading = false;
          }
        }
      )
  }

}
