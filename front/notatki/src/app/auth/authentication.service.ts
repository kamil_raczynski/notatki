import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {ConfigService} from "../core/config.service";

@Injectable()
export class AuthenticationService {
  public token: string;
  private apiUrl:string;

  constructor(
    private http: Http,
    private configService: ConfigService
  ) {
    // set token if saved in local storage
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
    this.apiUrl = this.configService.getConfig().apiUrl;
  }

  login(email: string, password: string): Observable<boolean> {
    let loginHeaders = new Headers({
      'Content-Type': 'application/json'
    });
    // var creds = "email=" + email + "&password=" + password;
    let creds = `{"email":"${email}","password":"${password}"}`;
    return this.http.post(
      `${this.apiUrl}/user/login`,
      creds,
      {headers: loginHeaders})
      .map((response: Response) => {
        let token = response.json() && response.json().token;
        if (token) {
          this.token = token;
          localStorage.setItem('currentUser', JSON.stringify({ email: email, token: token }));
          return true;
        } else {
          return false;
        }
      });
  }

  register(form) {
    let registerHeaders = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(`${this.apiUrl}/user/register`, form, {headers: registerHeaders})
      .map((response: Response) => {
        let token = response.json() && response.json().token;
        if (token){
          this.token = token;
          localStorage.setItem('currentUser', JSON.stringify({email: form.email, token: token}));
          return true;
        }else{
          return false;
        }
      })
  }

  logout(): void {
    let logoutHeaders = new Headers({
      'Authorization': 'Bearer ' + this.token
    });
    if (this.token) {
      this.http.get(`${this.apiUrl}/user/logout`, {headers: logoutHeaders})
        .map(result => result)
        .subscribe(
          (data) => {
            this.token = null;
            localStorage.removeItem('currentUser');
          },
          (error) => {
            alert(error);
          }
        );
    }
  }

}
