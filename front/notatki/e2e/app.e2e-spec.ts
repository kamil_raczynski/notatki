import { NotatkiPage } from './app.po';

describe('notatki App', function() {
  let page: NotatkiPage;

  beforeEach(() => {
    page = new NotatkiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
