# ReadMe #

## Front app ##

To build app you need to install:

* {node, node} <- https://docs.npmjs.com/getting-started/installing-node
* angular-cli <- https://github.com/angular/angular-cli

After installation:

```
cd front
cd notatki
ng serve
```
then open in browser: http://localhost:4200/